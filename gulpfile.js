"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var server = require("browser-sync").create();
var svgstore = require('gulp-svgstore');

gulp.task("style", function() {
  gulp.src("app/sass/style.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({browsers: [
        "last 2 versions"
      ]})
    ]))
    .pipe(gulp.dest("app/css"))
    .pipe(server.stream());
});

gulp.task("serve", ["style"], function() {
  server.init({
    baseDir: "app/",
    server: "app/",
    index: "form.html",
    ghostMode: false,
    notify: false,
    open: true,
    cors: true,
    ui: false,
  });

  gulp.watch("app/sass/**/*.{scss,sass}", ["style"]);
  gulp.watch("app/*.html").on("change", server.reload);
});

gulp.task("copy", function () {
  return gulp.src ([
    "fonts/**/*.{woff,woff2}",
    "img/**",
    "js/**",
    "css/**",
    "*.html"
  ], {
    base: "."
  })
});

gulp.task('svgstore', function () {
  var svgs = gulp
      .src('app/*.svg')
      .pipe(svgstore({ inlineSvg: true }));

  function fileContents (filePath, file) {
      return file.contents.toString();
  }

  return gulp
      .src('app/inline-svg.html')
      .pipe(inject(svgs, { transform: fileContents }))
      .pipe(gulp.dest('app/dest'));
});